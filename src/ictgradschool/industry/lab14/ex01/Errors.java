package ictgradschool.industry.lab14.ex01;

/**
 * Created by ylin183 on 2/05/2017.
 */

import java.awt.event.*;
import java.util.concurrent.ExecutionException;
import javax.swing.*;

public class Errors implements ActionListener {
    private JLabel progressLabel = new JLabel();
    private JLabel myLabel = new JLabel();
    private JButton myButton = new JButton();

    /**
     * Called when the button is clicked.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        myButton.setEnabled(false);
// Start the SwingWorker running
        MySwingWorker worker = new MySwingWorker();
        worker.execute();

        try {
            worker.doInBackground();
        } catch (Exception error) {
            error.printStackTrace();
        }

// When the SwingWorker has finished, display the result in myLabel.
        int result = 0;
        try {
            result = worker.get();
        } catch (InterruptedException error) {
            error.printStackTrace();
        } catch (ExecutionException error) {
            error.printStackTrace();
        }
        myButton.setEnabled(true);
        myLabel.setText("Result: " + result);
    }

    private class MySwingWorker extends SwingWorker<Integer, Void> {
        protected Integer doInBackground() throws Exception {
            int result = 0;
            for (int i = 0; i < 100; i++) {
// Do some long-running stuff
                result += doStuffAndThings();
// Report intermediate results
                progressLabel.setText("Progress: " + i + "%");
            }
            return result;
        }

        // Assume doStuffAndThings() method performs some task returning int
        private int doStuffAndThings() {
            return 0;
        }
    }
}
