package ictgradschool.industry.lab14.ex03;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

/**
 * Simple application to calculate the prime factors of a given number N.
 * 
 * The application allows the user to enter a value for N, and then calculates 
 * and displays the prime factors. This is a very simple Swing application that
 * performs all processing on the Event Dispatch thread.
 *
 */
public class CancellablePrimeFactorsSwingApp extends JPanel implements ActionListener {

	// Declare instance variables for accessing inside class ***
	private JButton _startBtn;        // Button to start the calculation process.
	private JButton _stopBtn; // ***
	private JTextArea _factorValues;  // Component to display the result.
    private JTextField tfN; // ***
    private PrimeFactorisationWorker primeFactorisationWorker; // ***

    /// SwingWorker class start
    private class PrimeFactorisationWorker extends SwingWorker<List<Long>,Void> {

        // Declare n and construct

        long n;

        PrimeFactorisationWorker(long n) {
            this.n = n;
        }

        @Override
        public List<Long> doInBackground() throws Exception {

            long startTime = System.currentTimeMillis();

            List<Long> list = new ArrayList<>();

            // Start the computation in the Event Dispatch thread.

            for (long i = 2; i * i <= n; i++) {

                if (!isCancelled()) {
                    // If i is a factor of N, repeatedly divide it out
                    while (n % i == 0) {
                        list.add(i);
                        n = n / i;
                    }
                } else {
                    return null;
                }
            }

            // if biggest factor occurs only once, n > 1
            if (n > 1) {
                list.add(n);
            }

            long endTime = System.currentTimeMillis();

            System.out.println("Running time in seconds: " + (double) (endTime - startTime) / 1000);
            System.out.println("Running time in ms: " + (endTime - startTime));

            return list;
        }

        @Override
        public void done() {

            // Re-enable the Start button.
            _startBtn.setEnabled(true);
            _stopBtn.setEnabled(false);

            // Restore the cursor.
            setCursor(Cursor.getDefaultCursor());

            try {
                List<Long> longList = get();
                if (isDone()) {

                    String longstr = "";

                    for (long i : longList) {
                        longstr += i + "\n";
                    }
                    _factorValues.setText(longstr);

                }
            } catch (final InterruptedException | ExecutionException e) {
//                throw new RuntimeException(e.getCause());
                System.out.println("Error");
            } catch (final CancellationException e) {
                System.out.println("Operation cancelled");
            }
        }
    }
    /// SwingWorker class ends

	public CancellablePrimeFactorsSwingApp() {
		// Create the GUI components.
		JLabel lblN = new JLabel("Value N:");
		tfN = new JTextField(20);

		// Make buttons and textarea ***
		_startBtn = new JButton("Compute");
		_startBtn.addActionListener(this); // ***
		_stopBtn = new JButton("Abort"); // ***
        _stopBtn.addActionListener(this); // ***
		_factorValues = new JTextArea();
		_factorValues.setEditable(false);

		// Construct the GUI. 
		JPanel controlPanel = new JPanel();
		controlPanel.add(lblN);
		controlPanel.add(tfN);
		controlPanel.add(_startBtn);
		controlPanel.add(_stopBtn); // ***
		
		JScrollPane scrollPaneForOutput = new JScrollPane();
		scrollPaneForOutput.setViewportView(_factorValues);
		
		setLayout(new BorderLayout());
		add(controlPanel, BorderLayout.NORTH);
		add(scrollPaneForOutput, BorderLayout.CENTER);
		setPreferredSize(new Dimension(500,300));
	}

	private static void createAndShowGUI() {
		// Create and set up the window.
		JFrame frame = new JFrame("Prime Factorisation of N");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Create and set up the content pane.
		JComponent newContentPane = new CancellablePrimeFactorsSwingApp();
		frame.add(newContentPane);

		// Display the window.
		frame.pack();
        frame.setLocationRelativeTo(null); 
		frame.setVisible(true);
	}

    @Override
    public void actionPerformed(ActionEvent event) {
        if (event.getSource() == _startBtn) {
            String strN = tfN.getText().trim();
            long n = 0;

            try {
                n = Long.parseLong(strN);
            } catch(NumberFormatException e) {
                System.out.println(e);
            }

            // Disable the Start button until the result of the calculation is known.
            _startBtn.setEnabled(false);
            _stopBtn.setEnabled(true);

            // Clear any text (prime factors) from the results area.
            _factorValues.setText(null);

            // Set the cursor to busy.
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

            /// Run SwingWorker
            primeFactorisationWorker = new PrimeFactorisationWorker(n);
            primeFactorisationWorker.execute();
        }

        else if (event.getSource() == _stopBtn) {
            primeFactorisationWorker.cancel(true);
            primeFactorisationWorker = null;
        }
    }

	public static void main(String[] args) {
		// Schedule a job for the event-dispatching thread:
		// creating and showing this application's GUI.
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}

