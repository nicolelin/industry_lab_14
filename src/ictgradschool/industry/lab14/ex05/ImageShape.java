package ictgradschool.industry.lab14.ex05;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

/**
 * A shape which is capable of loading and rendering images from files / Uris / URLs / etc.
 */
public class ImageShape extends Shape {

    private Image image;

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, String fileName) throws MalformedURLException {
        this(x, y, deltaX, deltaY, width, height, new File(fileName).toURI());
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, URI uri) throws MalformedURLException {
        this(x, y, deltaX, deltaY, width, height, uri.toURL());
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, URL url) {
        super(x, y, deltaX, deltaY, width, height);

        LoadImage loadImage = new LoadImage(url, width, height);
        loadImage.execute();
    }


    @Override
    public void paint(Painter painter) {

        // If image is not loaded, return red rectangle + text

        // If image is loaded, paint

        if (this.image == null) {
            painter.setColor(Color.lightGray);
            painter.fillRect(fX, fY, fWidth, fHeight);
            painter.setColor(Color.red);
            painter.drawRect(fX, fY, fWidth, fHeight);
            painter.setColor(Color.red);
            painter.drawCenteredText("Loading", fX, fY, fWidth, fHeight);
        } else {
            painter.drawImage(this.image, fX, fY, fWidth, fHeight);
        }
    }


    class LoadImage extends SwingWorker<Image, Void> {

        private final URL url;
        private final int width;
        private final int height;

        public LoadImage(URL url, int width, int height) {
            super();

            this.url = url;
            this.width = width;
            this.height = height;

            System.out.println("DEBUG: loading image");

        }

        @Override
        protected Image doInBackground() throws IOException {
            try {
                image = ImageIO.read(url);
                if (width == image.getWidth(null) && height == image.getHeight(null)) {

                } else {
                    image = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
                }
                image.getHeight(null);

            } catch (IOException e) {
                e.printStackTrace();
            }
            return image;
        }
    }
}
